package typescript

import (
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/cmd"
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/util"
	"path/filepath"
)

func Run(files []string, stdin string) (string, string, error) {
	stdout, stderr, err := Compile(files, stdin)
	if err != nil {
		return stdout, stderr, err
	}

	return RunTests(files, stdin)
}

func RunTests(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])
	jsName := "a.js"

	return cmd.RunStdinTest(workDir, stdin, "node", jsName)
}

func Compile(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])
	jsName := "a.js"

	// Find all typescript files and build compile command
	sourceFiles := util.FilterByExtension(files, "ts")
	args := append([]string{"tsc", "-out", jsName}, sourceFiles...)

	// Compile to javascript
	return cmd.Run(workDir, args...)
}