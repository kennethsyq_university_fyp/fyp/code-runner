package csharp

import (
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/cmd"
	"path/filepath"
)

func Run(files []string, stdin string) (string, string, error) {
	stdout, stderr, err := Compile(files, stdin)
	if err != nil {
		return stdout, stderr, err
	}

	return RunTests(files, stdin)
}

func RunTests(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])

	// TODO: Memory Limit causes crash (https://github.com/dotnet/runtime/issues/13027)
	return cmd.RunStdinTestCPU(workDir, stdin, "dotnet", "bin/Debug/net*/*.dll")
}

func Compile(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])
	fName := filepath.Base(files[0])
	defaultFn := false
	if fName == "Program" {
		// rename it temporary
		_, _, _ = cmd.Run(workDir, "mv", "Program.cs", "Program-tmp.cs")
		defaultFn = true
	}

	// Setup Project
	_, _, _ = cmd.Run(workDir, "dotnet", "new", "console")
	_, _, _ = cmd.Run(workDir, "rm", "Program.cs")

	if defaultFn {
		_, _, _ = cmd.Run(workDir, "mv", "Program-tmp.cs", "Program.cs")
	}

	// Build
	return cmd.Run(workDir, "dotnet", "build")
}