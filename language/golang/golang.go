package golang

import (
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/cmd"
	"path/filepath"
)

func Run(files []string, stdin string) (string, string, error) {
	stdout, stderr, err := Compile(files, stdin)
	if err != nil {
		return stdout, stderr, err
	}

	return RunTests(files, stdin)
}

func RunTests(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])
	return cmd.RunStdinTest(workDir, stdin, "./test")
}

func Compile(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])
	return cmd.Run(workDir, "go", "build", "-o", "test", files[0])
}