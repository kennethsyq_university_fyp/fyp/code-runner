package erlang

import (
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/cmd"
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/util"
	"path/filepath"
	"strconv"
)

func Run(files []string, stdin string) (string, string, error) {
	stdout, stderr, err := Compile(files, stdin)
	if err != nil {
		return stdout, stderr, err
	}

	return RunTests(files, stdin)
}

func RunTests(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])

	// Run first file with escript
	memlim := "-Xmx" + strconv.Itoa(util.Memlimit) + "k"
	cmd.RunBash(workDir, "export _JAVA_OPTIONS=\"" + memlim + "\"") // Set memory limit
	return cmd.RunStdinTestCPU(workDir, stdin, "escript", files[0])
}

func Compile(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])

	// Compile all files except the first
	for _, file := range files[1:] {
		stdout, stderr, err := cmd.Run(workDir, "erlc", file)
		if err != nil {
			return stdout, stderr, err
		}
	}
	return "Success", "", nil
}