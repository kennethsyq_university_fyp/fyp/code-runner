package elixir

import (
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/cmd"
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/util"
	"path/filepath"
)

func Run(files []string, stdin string) (string, string, error) {
	return RunTests(files, stdin)
}

func RunTests(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])
	sourceFiles := util.FilterByExtension(files, "ex")
	args := append([]string{"elixirc"}, sourceFiles...)
	return cmd.RunStdinTest(workDir, stdin, args...)
}

func Compile(files []string, stdin string) (string, string, error) {
	return "", "", nil // NO-OP
}