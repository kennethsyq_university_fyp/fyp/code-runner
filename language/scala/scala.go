package scala

import (
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/cmd"
	"path/filepath"
)

func Run(files []string, stdin string) (string, string, error) {
	stdout, stderr, err := Compile(files, stdin)
	if err != nil {
		return stdout, stderr, err
	}

	return RunTests(files, stdin)
}

func RunTests(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])

	return cmd.RunStdinTest(workDir, stdin, "scala", "Main")
}

func Compile(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])

	args := append([]string{"scalac"}, files...)
	return cmd.Run(workDir, args...)
}