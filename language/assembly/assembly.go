package assembly

import (
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/cmd"
	"path/filepath"
)

func Run(files []string, stdin string) (string, string, error) {
	stdout, stderr, err := Compile(files, stdin)
	if err != nil {
		return stdout, stderr, err
	}

	return RunTests(files, stdin)
}

func RunTests(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])
	objName := "a.o"
	binName := "a.out"

	stdout, stderr, err := cmd.Run(workDir, "ld", "-o", binName, objName)
	if err != nil {
		return stdout, stderr, err
	}

	binPath := filepath.Join(workDir, binName)
	_, _, _ = cmd.Run(workDir, "chmod", "+x", binPath)
	return cmd.RunStdinTest(workDir, stdin, binPath)
}

func Compile(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])
	objName := "a.o"

	return cmd.Run(workDir, "nasm", "-f", "elf64", "-o", objName, files[0])
}