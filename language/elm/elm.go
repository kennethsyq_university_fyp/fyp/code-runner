package elm

import (
	"path/filepath"

	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/cmd"
)

func Run(files []string, stdin string) (string, string, error) {
	stdout, stderr, err := Compile(files, stdin)
	if err != nil {
		return stdout, stderr, err
	}

	return RunTests(files, stdin)
}

func RunTests(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])

	return cmd.RunStdinTest(workDir, stdin, "node", "app.js")
}

func Compile(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])

	// Move bootstrap files into work dir
	stdout, stderr, err := cmd.RunBash(workDir, "cp -rf /bootstrap/* .")
	if err != nil {
		return stdout, stderr, err
	}

	// Compile elm to javascript
	return cmd.Run(workDir, "elm", "make", files[0], "--output", "elm.js")
}