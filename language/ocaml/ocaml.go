package ocaml

import (
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/cmd"
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/util"
	"path/filepath"
)

func Run(files []string, stdin string) (string, string, error) {
	stdout, stderr, err := Compile(files, stdin)
	if err != nil {
		return stdout, stderr, err
	}

	return RunTests(files, stdin)
}

func reverse(files []string) []string {
	reversed := make([]string, 0, len(files))

	for i := len(files) - 1; i >= 0; i-- {
		reversed = append(reversed, files[i])
	}

	return reversed
}

func RunTests(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])
	binName := "a"

	binPath := filepath.Join(workDir, binName)
	_, _, _ = cmd.Run(workDir, "chmod", "+x", binPath)
	return cmd.RunStdinTest(workDir, stdin, binPath)
}

func Compile(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])
	binName := "a"

	sourceFiles := reverse(util.FilterByExtension(files, "ml"))
	args := append([]string{"ocamlc", "-o", binName}, sourceFiles...)
	return cmd.Run(workDir, args...)
}