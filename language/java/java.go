package java

import (
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/cmd"
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/util"
	"path/filepath"
	"strconv"
)

func Run(files []string, stdin string) (string, string, error) {
	stdout, stderr, err := Compile(files, stdin)
	if err != nil {
		return stdout, stderr, err
	}

	return RunTests(files, stdin)
}

func className(fname string) string {
	ext := filepath.Ext(fname)
	return fname[0 : len(fname)-len(ext)]
}

func RunTests(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])
	fname := filepath.Base(files[0])

	memlim := "-Xmx" + strconv.Itoa(util.Memlimit) + "k"
	return cmd.RunStdinTestCPU(workDir, stdin, "java", memlim, className(fname))
}

func Compile(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])
	fname := filepath.Base(files[0])

	return cmd.Run(workDir, "javac", fname)
}
