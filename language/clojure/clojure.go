package clojure

import (
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/cmd"
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/util"
	"path/filepath"
	"strconv"
)

func Run(files []string, stdin string) (string, string, error) {
	return RunTests(files, stdin)
}

func RunTests(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])

	memlim := "-Xmx" + strconv.Itoa(util.Memlimit) + "k"
	return cmd.RunStdinTestCPU(workDir, stdin, "java", memlim, "-cp", "/usr/share/java/clojure.jar", "clojure.main", files[0])
}

func Compile(files []string, stdin string) (string, string, error) {
	return "", "", nil // NO-OP
}