package ruby

import (
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/cmd"
	"path/filepath"
)

func Run(files []string, stdin string) (string, string, error) {
	return RunTests(files, stdin)
}

func RunTests(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])
	return cmd.RunStdinTest(workDir, stdin, "ruby", files[0])
}

func Compile(files []string, stdin string) (string, string, error) {
	return "", "", nil // NO-OP
}