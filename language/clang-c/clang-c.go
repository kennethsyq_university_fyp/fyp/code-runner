package clang_c

import (
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/cmd"
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/util"
	"path/filepath"
)

func Run(files []string, stdin string) (string, string, error) {
	stdout, stderr, err := Compile(files, stdin)
	if err != nil {
		return stdout, stderr, err
	}

	return RunTests(files, stdin)
}

func RunTests(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])
	binName := "a.out"
	binPath := filepath.Join(workDir, binName)
	_, _, _ = cmd.Run(workDir, "chmod", "+x", binPath)

	return cmd.RunStdinTest(workDir, stdin, binPath)
}

func Compile(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])
	binName := "a.out"

	sourceFiles := util.FilterByExtension(files, "c")
	args := append([]string{"clang", "-o", binName, "-lm"}, sourceFiles...)
	return cmd.Run(workDir, args...)
}