package cmd

import (
	"bytes"
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/util"
	"os/exec"
	"strconv"
	"strings"
)

func Run(workDir string, args ...string) (string, string, error) {
	return RunStdin(workDir, "", args...)
}

func RunStdin(workDir, stdin string, args ...string) (string, string, error) {
	var stdout bytes.Buffer
	var stderr bytes.Buffer

	cmd := exec.Command(args[0], args[1:]...)
	cmd.Dir = workDir
	cmd.Stdin = strings.NewReader(stdin)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()

	return stdout.String(), stderr.String(), err
}

func runStdinTestInternal(workDir, stdin string, opt []string) (string, string, error) {
	// Error code 124 = timeout
	arg := strings.Join(opt, " ")
	//fmt.Println(arg)

	var stdout bytes.Buffer
	var stderr bytes.Buffer

	cmd := exec.Command("bash", "-c", arg)
	cmd.Dir = workDir
	cmd.Stdin = strings.NewReader(stdin)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()

	return stdout.String(), stderr.String(), err
}

// Run with CPU and Memory limits default
func RunStdinTest(workDir, stdin string, args ...string) (string, string, error) {
	// Error code 124 = timeout
	opt := []string{"ulimit", "-St", strconv.Itoa(util.Cpulimit), "-Sv", strconv.Itoa(util.Memlimit), "&&"}
	opt = append(opt, args...)
	//fmt.Println(opt)
	return runStdinTestInternal(workDir, stdin, opt)
}

// Run with memory limits default and custom CPU limit commad
func RunStdinTestCPU(workDir, stdin string, args ...string) (string, string, error) {
	// Error code 124 = timeout
	opt := []string{"ulimit", "-St", strconv.Itoa(util.Cpulimit), "&&"}
	opt = append(opt, args...)
	//fmt.Println(opt)
	return runStdinTestInternal(workDir, stdin, opt)
}

// Run a bash command
func RunBash(workDir, command string) (string, string, error) {
	return Run(workDir, "bash", "-c", command)
}

// Run a bash command with input
func RunBashStdin(workDir, command, stdin string) (string, string, error) {
	return RunStdin(workDir, stdin, "bash", "-c", command)
}
