package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/cmd"
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/language"
	"gitlab.com/kennethsyq_university_fyp/fyp/code-runner/v2/util"
)

type Payload struct {
	Language string          `json:"language"`
	Files    []*InMemoryFile `json:"files"`
	Stdin    string          `json:"stdin"`
	Command  string          `json:"command"`
}

type InMemoryFile struct {
	Name    string `json:"name"`
	Content string `json:"content"`
}

type Result struct {
	Stdout string `json:"stdout"`
	Stderr string `json:"stderr"`
	Error  string `json:"error"`
}

func main() {
	isKubernetesInt := util.GetEnvDefaultInt("KUBE_MODE", 0)
	payload := &Payload{}

	if isKubernetesInt == 0 {
		err := json.NewDecoder(os.Stdin).Decode(payload)
		if err != nil {
			exitF("Failed to parse input json (%s)\n", err.Error())
		}
	} else {
		stringload := util.GetEnvDefault("KUBE_DATA", "")
		if stringload == "" {
			exitF("No Json data in KUBE_DATA environment")
		}
		err := json.NewDecoder(strings.NewReader(stringload)).Decode(payload)
		if err != nil {
			exitF("Failed to parse input json (%s)\n", err.Error())
		}
	}

	minioEnabled := strings.ToUpper(util.GetEnvDefault("S3_STORE", "FALSE")) == "TRUE"
	var minioClient *minio.Client
	var minioBucket, subfolder string
	mode := 0 // 0 - Run, 1 - Compile, 2 - Test
	if minioEnabled {
		// Get various MinIO values
		minioEndpoint := util.GetEnvDefault("S3_ENDPOINT", "localhost")
		minioAccessKey := util.GetEnvDefault("S3_ACCESS_ID", "")
		minioSecretKey := util.GetEnvDefault("S3_SECRET_KEY", "")
		minioSSL := strings.ToUpper(util.GetEnvDefault("S3_SSL", "FALSE")) == "TRUE"
		mode = util.GetEnvDefaultInt("COMPILE_MODE", 0)
		minioBucket = util.GetEnvDefault("S3_BUCKET", "compiler")
		location := util.GetEnvDefault("S3_LOCATION", "ap-southeast-1")
		subfolder = util.GetEnvDefault("COMPILE_FOLDER_PATH", "") // Path to folder for download and path to main folder for upload

		mic, err := minio.New(minioEndpoint, &minio.Options{
			Creds:  credentials.NewStaticV4(minioAccessKey, minioSecretKey, ""),
			Secure: minioSSL,
		})
		if err != nil {
			// We ignore MinIO and presume normal ops
			mode = 0
		}
		if subfolder == "" {
			// Ignore MinIO
			mode = 0
		}
		minioClient = mic
		ctx := context.Background()

		err = minioClient.MakeBucket(ctx, minioBucket, minio.MakeBucketOptions{Region: location})
		if err != nil {
			// Check to see if we already own this bucket (which happens if you run this twice)
			exists, errBucketExists := minioClient.BucketExists(ctx, minioBucket)
			if !(errBucketExists == nil && exists) {
				exitF(err.Error())
			}
		}
	}

	// Ensure that we have at least one file
	if len(payload.Files) == 0 {
		exitF("No files given\n")
	}

	// Check if we support given language
	if !language.IsSupported(payload.Language) {
		exitF("Language '%s' is not supported\n", payload.Language)
	}

	// Write files to disk
	filepaths, err, tmpPath := writeFiles(payload.Files, mode)
	if err != nil {
		exitF("Failed to write file to disk (%s)", err.Error())
	}

	util.UpdateEnvLimits()

	var stdout, stderr string

	// Execute the given command or run the code with
	// the language runner if no command is given
	if payload.Command == "" {
		switch mode {
		case 1:
			stdout, stderr, err = language.Compile(payload.Language, filepaths, payload.Stdin)
			totalSize, bucketPath := uploadFilesFromTmp(tmpPath, minioClient, minioBucket, subfolder)
			stdout = "Uploaded|%|" + bucketPath + "|%|" + strconv.FormatInt(totalSize, 10) // Replace stdout with this as we are not running tests here
			break
		case 2:
			filepaths, err = downloadFromCache(minioClient, subfolder, minioBucket)
			stdout, stderr, err = language.Test(payload.Language, filepaths, payload.Stdin)
			break
		default:
			stdout, stderr, err = language.Run(payload.Language, filepaths, payload.Stdin)
			break
		}
	} else {
		workDir := filepath.Dir(filepaths[0])
		stdout, stderr, err = cmd.RunBashStdin(workDir, payload.Command, payload.Stdin)
	}
	printResult(stdout, stderr, err)
}

// Upload to MinIO
func uploadFilesFromTmp(tmpPath string, client *minio.Client, bucket string, folderPath string) (int64, string) {
	var files []string
	ctx := context.Background()

	err := filepath.Walk(tmpPath, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			files = append(files, path)
		}
		return nil
	})
	if err != nil {
		panic(err)
	}
	var uploadedSize int64
	for _, file := range files {
		f, err := os.Open(file)
		if err != nil {
			exitF("Cannot open file " + file + " | " + err.Error())
		}
		fileType, err := util.GetFileContentType(f)
		if err != nil {
			fileType = "text/plain" // Presume plain text as we cannot detect content type
		}

		filePath := folderPath + file
		//fmt.Println("Saving to " + filePath + " with Content-Type " + fileType)
		n, err := client.FPutObject(ctx, bucket, filePath, file, minio.PutObjectOptions{ContentType: fileType})
		if err != nil {
			exitF(err.Error())
		}
		uploadedSize += n.Size
	}
	return uploadedSize, folderPath + tmpPath
}

// Download from MinIO
func downloadFromCache(minioClient *minio.Client, minioPath string, bucket string) ([]string, error) {
	filesToDl := make([]string, 0)
	ctx := context.Background()
	for object := range minioClient.ListObjects(ctx, bucket, minio.ListObjectsOptions{Recursive: true, Prefix: minioPath}) {
		if object.Err != nil {
			fmt.Println(object.Err)
			return nil, nil
		}
		filesToDl = append(filesToDl, object.Key)
	}
	// Create temp dir
	tmpPath, err := ioutil.TempDir("", "")
	if err != nil {
		return nil, err
	}
	paths := make([]string, len(filesToDl), len(filesToDl))
	for i, key := range filesToDl {
		// Remove the minioPath and replace as new path
		newPath := tmpPath + strings.Replace(key, minioPath, "", -1)
		err := os.MkdirAll(filepath.Dir(newPath), 0775)
		if err != nil {
			return nil, err
		}
		err = minioClient.FGetObject(ctx, bucket, key, newPath, minio.GetObjectOptions{})
		if err != nil {
			exitF("Unable to download object from cache: " + key)
		}
		paths[i] = newPath
	}

	return paths, nil
}

// Writes files to disk, returns list of absolute filepaths
func writeFiles(files []*InMemoryFile, mode int) ([]string, error, string) {
	if mode > 1 {
		return nil, nil, "" // NOOP as we are not writing anything to tempdir
	}
	// Create temp dir
	tmpPath, err := ioutil.TempDir("", "")
	if err != nil {
		return nil, err, ""
	}

	paths := make([]string, len(files), len(files))
	for i, file := range files {
		path, err := writeFile(tmpPath, file)
		if err != nil {
			return nil, err, ""
		}

		paths[i] = path

	}
	return paths, nil, tmpPath
}

// Writes a single file to disk
func writeFile(basePath string, file *InMemoryFile) (string, error) {
	// Get absolute path to file inside basePath
	absPath := filepath.Join(basePath, file.Name)

	// Create all parent dirs
	err := os.MkdirAll(filepath.Dir(absPath), 0775)
	if err != nil {
		return "", err
	}

	// Write file to disk
	err = ioutil.WriteFile(absPath, []byte(file.Content), 0664)
	if err != nil {
		return "", err
	}

	// Return absolute path to file
	return absPath, nil
}

func exitF(format string, a ...interface{}) {
	_, _ = fmt.Fprintf(os.Stderr, format, a...)
	os.Exit(1)
}

func printResult(stdout, stderr string, err error) {
	result := &Result{
		Stdout: stdout,
		Stderr: stderr,
		Error:  errToStr(err),
	}
	_ = json.NewEncoder(os.Stdout).Encode(result)
}

func errToStr(err error) string {
	if err != nil {
		return err.Error()
	}

	return ""
}
