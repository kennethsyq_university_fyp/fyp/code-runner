package util

import (
	"net/http"
	"os"
	"path/filepath"
	"strconv"
)

var Memlimit int // in KB
var Cpulimit int

// Filter by file extension type
func FilterByExtension(files []string, ext string) []string {
	var newFiles []string
	suffix := "." + ext

	for _, file := range files {
		if filepath.Ext(file) == suffix {
			newFiles = append(newFiles, file)
		}
	}

	return newFiles
}

// Update various environment limits
func UpdateEnvLimits() {
	Memlimit = GetEnvDefaultInt("MEM_LIMIT", 1024) * 1000 // Hard 1GB fallback if memory limit not defined by master controller
	Cpulimit = GetEnvDefaultInt("CPU_TIME_LIMIT", 60) // Hard 60 seconds fallback if CPU time limit not defined by master controller
}

// Get environment variable as string with default value if not found
func GetEnvDefault(key string, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

// Get environment variable as int with default value if not found
func GetEnvDefaultInt(key string, fallback int) int {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	val, err := strconv.Atoi(value)
	if err != nil {
		return fallback
	}
	return val
}

// Get file content type to determine what the file is
func GetFileContentType(out *os.File) (string, error) {

	// Only the first 512 bytes are used to sniff the content type.
	buffer := make([]byte, 512)

	_, err := out.Read(buffer)
	if err != nil {
		return "", err
	}

	// Use the net/http package's handy DectectContentType function. Always returns a valid
	// content-type by returning "application/octet-stream" if no others seemed to match.
	contentType := http.DetectContentType(buffer)

	return contentType, nil
}