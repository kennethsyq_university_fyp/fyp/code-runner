image: alpine:latest

stages:
  - build
  - test
  - upload
  - deploy

include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Secret-Detection.gitlab-ci.yml
  - template: Code-Quality.gitlab-ci.yml

workflow:
  rules:
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH

secret_detection:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success
  needs: []

secret_detection_default_branch:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success
  needs: [ ]

gosec-sast:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success
  needs: [ ]

code_quality:
  artifacts:
    paths: [gl-code-quality-report.json]
  rules:
    - if: '$CODE_QUALITY_DISABLED'
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success
  needs: []

code_quality_html:
  extends: code_quality
  variables:
    REPORT_FORMAT: html
  artifacts:
    paths: [gl-code-quality-report.html]
  rules:
    - if: '$CODE_QUALITY_DISABLED'
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success
  needs: [ ]

.multiarchbuildcommon:
  stage: build
  image: golang:latest
  variables:
    GOOS: linux
  before_script:
    - mkdir /go/src/app
    - mv * /go/src/app
    - cd /go/src/app
    - go mod download
  script: go build -ldflags '-w -s' -o runner
  artifacts:
    expire_in: 14 days

Build Runner x64:
  extends: .multiarchbuildcommon
  variables:
    GOARCH: amd64
  after_script:
    - mv /go/src/app/runner ./runner-amd64
    - ls -l
  artifacts:
    paths:
      - runner-amd64

Build Runner ARM:
  extends: .multiarchbuildcommon
  variables:
    GOARCH: arm
  after_script:
    - mv /go/src/app/runner ./runner-arm
    - ls -l
  artifacts:
    paths:
      - runner-arm

Build Runner ARM64:
  extends: .multiarchbuildcommon
  variables:
    GOARCH: arm64
  after_script:
    - mv /go/src/app/runner ./runner-arm64
    - ls -l
  artifacts:
    paths:
      - runner-arm64

variables:
  PACKAGE_VERSION: "1.2.8"
  ARM64_BINARY: "coderunner-arm64-${PACKAGE_VERSION}"
  ARM_BINARY: "coderunner-arm-${PACKAGE_VERSION}"
  AMD64_BINARY: "coderunner-amd64-${PACKAGE_VERSION}"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/coderunner/${PACKAGE_VERSION}"

Upload Artifact:
  image: curlimages/curl:latest
  stage: upload
  dependencies:
    - Build Runner ARM64
    - Build Runner x64
    - Build Runner ARM
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file runner-arm64 ${PACKAGE_REGISTRY_URL}/${ARM64_BINARY}
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file runner-arm ${PACKAGE_REGISTRY_URL}/${ARM_BINARY}
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file runner-amd64 ${PACKAGE_REGISTRY_URL}/${AMD64_BINARY}
  rules:
    - if: $CI_COMMIT_TAG

Deploy Artifact:
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  stage: deploy
  dependencies:
    - Upload Artifact
  script:
    - echo "Releasing tagged release"
    - release-cli create --name "Release $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG --description "$CI_COMMIT_MESSAGE" --ref "$CI_COMMIT_TAG" \
      --assets-link "{\"name\":\"${AMD64_BINARY}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${AMD64_BINARY}\"}"
  #release:
    #tag_name: $CI_COMMIT_TAG
    #description: $CI_COMMIT_MESSAGE
    #ref: $CI_COMMIT_TAG
  rules:
    - if: $CI_COMMIT_TAG
